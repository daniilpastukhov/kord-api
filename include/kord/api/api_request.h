/*////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright 2022 by Kassow Robots, ApS
//
// The information contained herein is confidential, proprietary to Kassow Robots,
// ApS, and considered a trade secret as defined in section 263 and section 264
// under the Danish Criminal Code. Use of this information by anyone
// other than authorized employees of Kassow Robots, ApS is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// Authors: Ondrej Bruna  obr@kassowrobots.com
//
/////////////////////////////////////////////////////////////////////////////*/

#ifndef KR2_KORD_API_REQUEST_H
#define KR2_KORD_API_REQUEST_H

#pragma once

#include "kr2/kord/protocol/DataDescriptions/Requests/ControlCommandStatus.h"
#include "kr2/kord/protocol/DataDescriptions/Requests/ControlCommandItems.h"
#include "kr2/kord/protocol/KORDItemIDs.h"

#include <chrono>

namespace kr2 {
namespace kord {

namespace kkp = kr2::kord::protocol;

class Request
{
    public:
    // This is a unique identifier to the request (based on a nanosec timestamp)
    int64_t request_rid_{0};
    kkp::EKORDItemID request_type_{};
    kkp::EControlCommandItems system_request_type_{};
    kkp::EControlCommandStatus request_status_{};
    unsigned int error_code_{};

    Request& asSystem() {
        this->request_type_ = kkp::EKORDItemID::eRequestSystem;

        return *this;
    }
    // RequestIO& asIO();

    kkp::EControlCommandStatus getStatus() const{
        return request_status_;
    }
};

class RequestSystem : public Request{

    public:
    RequestSystem() { request_type_ = kkp::EKORDItemID::eRequestSystem; };
    RequestSystem(const Request&a_req): Request(a_req) { };

    /**
     * @brief Set command item to \b TransferLogFiles command and fills in the 
     * request identifier.
     * 
     * @return RequestSystem& 
     */
    RequestSystem& asLogTransfer()
    { 
        system_request_type_ = kkp::EControlCommandItems::eTransferLogFiles;
        request_rid_ = std::chrono::steady_clock::now().time_since_epoch().count();
        return *this;
    };

    /**
     * @brief Set command item to \b TransferDashboardJson command and fills in the 
     * request identifier.
     * 
     * @return RequestSystem& 
     */
    RequestSystem& asDashboardJsonTransfer()
    { 
        system_request_type_ = kkp::EControlCommandItems::eTransferDashboardJson;
        request_rid_ = std::chrono::steady_clock::now().time_since_epoch().count();
        return *this;
    };


    /**
     * @brief Set command item to \b TransferCalibrationData command and fills in the 
     * request identifier.
     * 
     * @return RequestSystem& 
     */
    RequestSystem& asCalibrationDataTransfer()
    { 
        system_request_type_ = kkp::EControlCommandItems::eTransferCalibrationData;
        request_rid_ = std::chrono::steady_clock::now().time_since_epoch().count();
        return *this;
    };



};

class RequestTransfer : public Request{

    public:

    uint32_t tranfer_mask_{0};

    RequestTransfer() { request_type_ = kkp::EKORDItemID::eRequestTransfer; };
    RequestTransfer(const Request&a_req): Request(a_req) { };

    /**
     * @brief Set command item to \b TransferFiles command and fills in the 
     * request identifier and files mask.
     * 
     * @return RequestTransfer& 
     */
    RequestTransfer& asMultipleFilesTransfer(uint32_t a_mask)
    { 
        system_request_type_ = kkp::EControlCommandItems::eTransferFiles;
        request_rid_ = std::chrono::steady_clock::now().time_since_epoch().count();
        tranfer_mask_ = a_mask;
        return *this;
    };


    /**
     * @brief Set command item to \b TransferFiles command and fills in the 
     * request identifier and files mask.
     * 
     * @return RequestTransfer& 
     */
    RequestTransfer& asFilesTransfer()
    {
        system_request_type_ = kkp::EControlCommandItems::eTransferFiles;
        request_rid_ = std::chrono::steady_clock::now().time_since_epoch().count();
        tranfer_mask_ = 0;
        return *this;
    };

    /**
     * @brief Add Logs to the \b TransferFiles files
     * 
     * 
     * @return RequestTransfer& 
     */
    RequestTransfer& withLogs()
    {
        tranfer_mask_ |= 1 << 0;
        return *this;
    };

    /**
     * @brief Add DashboardJson to the \b TransferFiles files
     * 
     * @return RequestTransfer& 
     */
    RequestTransfer& withDashboardJson()
    {
        tranfer_mask_ |= 1 << 1;
        return *this;
    };

    /**
     * @brief Add Calibration file, model.urdf, to the \b TransferFiles files
     * 
     * @return RequestTransfer& 
     */
    RequestTransfer& withCalibration()
    {
        tranfer_mask_ |= 1 << 2;
        return *this;
    };

};
}
}

#endif