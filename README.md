# KORD API
**KORD version 1.4.1**

This is client side API project of the KORD (Kassow Open Realtime Data). The purpose of this project is to provide an abstraction of the real-time access to Kassow Robots RC and its' control loop.

The project in progress now and more information will be added soon... stay tuned.

# Master CBun
For the KORD API to work and operate on external system, the master module has to be installed and activated in the RC. The module provides the connectivity and access to RC software. 

Download the Cbun from the [following link](https://gitlab.com/kassowrobots/kord-api/-/wikis/Master-CBun), place it on USB and install it into Kassow Robots RC using the Cbun Manager.



Please note the module is containing the actual source, so you can modify it for your own purposes. To do so rename the 'cbun' extension to 'tar.gz' and extract the content.

## Clone repository including submodules

If you would like to clone the repository with default kord protocol clone with `--recurse-submodules` option.

```
git clone --recurse-submodules  git@gitlab.com:kassowrobots/kord-api.git
```

## Documentation

Detailed documentation to recent API (v1.4.1) is available on the following public link:

[Documentation KORD Api 1.4.1](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.4.1)

For older fersions please refer to following links:

[Documentation KORD Api 1.4.0](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.4.0)

[Documentation KORD Api 1.3.0](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.3.0)

[Documentation KORD Api 1.2.0](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.2.0/)
