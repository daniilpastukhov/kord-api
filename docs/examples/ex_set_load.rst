
.. _ex-set_load:

Set Loads values **(new)**
==========================

.. note::
    Available in KORD v1.3
    
It is also possible to set different Frames and Loads using functions:

.. code-block:: c++

    // Sync MUST pass before any data can be fetched.
    kord->syncRC();
    rcv_iface.fetchData();
    
    //
    // Set LOAD_PAYLOAD/LOAD_2 values
    //
    std::array<double, 3UL> p = {0.3, 0.7, 0}; // preparing CoG
    std::array<double, 6UL> in = {0, 0, 0, 0.4 ,0.5 ,0.6}; // Inertia
    int64_t token; // token of the command to check its status
    ctl_iface.setLoad(kord::ELoadID::LOAD2, 12.3, p, in, token);
    
    // waiting for the command status-result
    while(rcv_iface.getCommandStatus(token) == -1){
        std::cout << "not found yet \n";
        if (!kord->waitSync(std::chrono::milliseconds(20))){
            std::cout << "Sync wait timed out, exit \n";
            break;
        }
        rcv_iface.fetchData(); // always fetching to see new values
    }

    // Review its values and evaluate them
