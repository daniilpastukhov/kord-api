Classes 
-------
   
kord::KordCore
~~~~~~~~~~~~~~
.. doxygenclass:: kr2::kord::KordCore 
  :members:
   
kord::ReceiverInterface
~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenclass:: kr2::kord::ReceiverInterface 
  :members:
   

kord::Connection_interface
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenclass:: kr2::kord::Connection_interface
   :members:

kord::ControlInterface
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenclass:: kr2::kord::ControlInterface
   :members:


