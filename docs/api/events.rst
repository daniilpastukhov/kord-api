
.. _events:

Events
------

kord::protocol::SystemEvent
~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenstruct:: kr2::kord::protocol::SystemEvent
   :members: 


.. _events-group:

kord::protocol::EEventGroup
~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::EEventGroup


.. _events-group-safety-event:

kord::protocol::ESafetyEventConditionID
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::ESafetyEventConditionID


.. _events-group-soft-stop-event:

kord::protocol::ESoftStopEventConditionID
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenenum:: kr2::kord::protocol::ESoftStopEventConditionID

    