
.. _installation-description:

************
Installation
************

Running the KORD requires two parts - an activated KORD CBun on the robot side and the KORD-API on the client (controller) side.

.. note::
    
    CBun for customers only. 
    
.. note::

    Real-time is only possible only for RT capable kernels. The examples can then be executed with RT priority provided by the user.

KORD CBun
=========

Needs to be installed, network set properly and activated.

Download the `KORD master CBun v0.0.5 <https://drive.google.com/uc?export=download&id=1bqlTDcoY-rOOwwvZKeOR76PSTN0izTSR>`_ and make it available to the robot.

Then install it to the robot by using the CBun Manager.


KORD-API
========

Getting the Source
------------------

The KORD-API is compiled from source code. The binaries are not provided.

.. note::
    The compilation was tested under Ubuntu *bionic*, *focal*, and *jammy*. We assume the build environment is already set up.

In order to get the source code you may refer to the `README.md <https://gitlab.com/kassowrobots/kord-api/-/blob/main/README.md>`_.

The project contains submodules and therefore use of the ``--recurse-submodules``.

.. code-block:: shell
    
    git clone --recurse-submodules https://gitlab.com/kassowrobots/kord-api.git

The directory ``kord-api`` should exist after running the command.

Compilation
-----------

Switch to the ``master`` branch.
To compile the KORD-API enter the cloned directory. Following example will work since ``cmake`` version 3.22.1.

.. code-block:: bash

    $ cd kord-api
    $ cmake -S . -B build
    $ cmake --build build

The compiled output is located in the build directory including examples to run.

.. warning::
    Please notice the KORD-API will not work if you do not have the CBun installed on the robot.

