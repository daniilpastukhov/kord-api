***
FAQ
***

* **Is KORD released yet?**

Yes. There is already stable KORD v1.2 available and we are continously working on improving and extending its functions.

* **Can I control the robot by directly talking to joints?**

Yes. It is possible, but be aware of that the feature is still expertimental. Take a look `Direct Control Movement` example.

