#!/bin/bash

DELETE_FOLDERS=(
    _doxy_out/
    _static/
    _build/
)

for item in ${DELETE_FOLDERS[@]}; do
echo "Checking: $item"
    if [ -d $item ]; then
        echo "Deleting: $item"
        rm -r $item
    fi
done

if [ "$1" == "clean" ]; then
    echo "Clean up only"
    exit 0
fi

echo "Compiling the documentation"

doxygen && mkdir _static && make html

exit 0
