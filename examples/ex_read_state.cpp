//
//  main.cpp
//  kord_prototype
//
//  Created by Martin Straka on 26.01.2022.
//

#include <iostream>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;

volatile bool stop = false;

void signal_handler( sig_atomic_t a_signum ) {
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

int main(int argc, char * argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        //lp.printUsage(true);
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(true);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));
    
    // insert code here...

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    // TODO: prepare initial q
    std::vector<double> q;
    q.resize(7);
    
    // Obtain initial q values
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";

    rcv_iface.fetchData();
    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);
    std::array<double, 7UL> sensed_trq = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_SENSED_TRQ);
    std::vector<std::variant<double, int>> tcp_to_wf = rcv_iface.getFrame(kord::EFrameID::TCP_FRAME,kord::EFrameValue::POSE_VAL_REF_WF);
    std::vector<std::variant<double, int>> tcp_fo_tfc = rcv_iface.getFrame(kord::EFrameID::TCP_FRAME,kord::EFrameValue::POSE_VAL_REF_TFC);
    std::vector<std::variant<double, int>> tfc_to_wf = rcv_iface.getFrame(kord::EFrameID::TFC_FRAME,kord::EFrameValue::POSE_VAL_REF_WF);
    std::vector<std::variant<double, int>> tfc_fo_tfc = rcv_iface.getFrame(kord::EFrameID::TFC_FRAME,kord::EFrameValue::POSE_VAL_REF_TFC);
    std::array<double, 6UL> get_tpc = rcv_iface.getTCP();

    std::cout << "Radians: [ ";
    for( double angl: start_q )
        std::cout << angl << " ";
    std::cout << "]" << std::endl;

    std::cout << "Degrees: [ ";
    for( double angl: start_q )
        std::cout << (angl * 180.0)/M_PI << " ";
    std::cout << "]" << std::endl;

    std::cout << "Sensed torques: [ ";
    for( double trq: sensed_trq )
        std::cout << trq << " ";
    std::cout << "]" << std::endl;

    std::cout << "TCP to WF: [ ";
    for( auto &p: tcp_to_wf )
        std::cout << std::get<double>(p) << " ";
    std::cout << "]" << std::endl;

    std::cout << "TCP to TFC: [ ";
    for( auto &p: tcp_fo_tfc )
        std::cout << std::get<double>(p) << " ";
    std::cout << "]" << std::endl;

    std::cout << "TFC to WF: [ ";
    for( auto &p: tfc_to_wf )
        std::cout << std::get<double>(p) << " ";
    std::cout << "]" << std::endl;

    std::cout << "TFC to TFC: [ ";
    for( auto &p: tfc_fo_tfc )
        std::cout << std::get<double>(p) << " ";
    std::cout << "]" << std::endl;
    
    std::cout << "TCP: [ ";
    for( double &p: get_tpc )
        std::cout << p << " ";
    std::cout << "]" << std::endl;
    
    // Read actual
    std::cout << "TCP: [";
    std::array<double, 6ul> tcp = rcv_iface.getTCP();
    for( double coord: tcp )
        std::cout << coord << " ";
    std::cout << "]" << std::endl;
    
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    std::cout << "Runtime: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() << "\n";
    std::cout << "Safety flags: " << rcv_iface.getRobotSafetyFlags() << "\n";
    std::cout << "System Alarm state: " << rcv_iface.systemAlarmState() << '\n';
        switch (rcv_iface.systemAlarmState() & 0b1111){
        case kr2::kord::protocol::EEventGroup::eUnknown:
            std::cout << "No alarms" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eSafetyEvent:
            std::cout << "Safety Event" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eSoftStopEvent:
            std::cout << "Soft Stop Event" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eKordEvent:
            std::cout << "Kord Event" << '\n';
            break;
    }

    std::cout << "HW flags: " << rcv_iface.getHWFlags() << "\n";
    std::cout << "Button flags: " << rcv_iface.getButtonFlags() << "\n";
    std::cout << "Motion flags: " << rcv_iface.getMotionFlags() << "\n";
    std::cout << "Safety  mode: " << rcv_iface.getSafetyMode() << "\n";
    std::cout << "Master speed: " << rcv_iface.getMasterSpeed() << "\n";
    std::cout << "Runtime, SF, SSTOP; PSTOP, ESTOP\n";
    std::chrono::time_point<std::chrono::steady_clock, std::chrono::seconds> print_stamp = std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::steady_clock::now());
    
    std::array<double, 7UL> acs_joints= rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_QDD);
    std::cout << "Joints accelerations: [ ";
    for( double ac: acs_joints )
        std::cout << ac << " ";
    std::cout << "]" << std::endl;

    // Infinite loop
    // while(true) {
    //     //
    //     // calculation
    //     // update q
    //     //

    //     // if (!kord->waitSync(std::chrono::milliseconds(10))){
    //     //     std::cout << "Sync wait timed out, exit \n";
    //     //     break;
    //     // }

    //     // rcv_iface.fetchData();
    //     auto now_ts = std::chrono::steady_clock::now();
    //     if ((print_stamp <  std::chrono::time_point_cast<std::chrono::seconds>(now_ts)) && (std::chrono::duration_cast<std::chrono::seconds>(now_ts - start).count() % 2))
    //     {
    //         print_stamp = std::chrono::time_point_cast<std::chrono::seconds>(now_ts);
    //         std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() << ", ";
    //         std::cout << rcv_iface.getRobotSafetyFlags() << ", ";
    //         std::cout << rcv_iface.systemAlarmState() << "\n";
    //     }

    //     if (lp.runtimeElapsed()){
    //         break;
    //     }
    //     if (stop) { break; }
    // }



    std::cout << std::endl;
    std::cout << "FailEmpty: " << rcv_iface.getStatistics(kord::ReceiverInterface::EStatsValue::FAIL_TO_READ_EMPTY)      << "\n";
    std::cout << "FailError: " << rcv_iface.getStatistics(kord::ReceiverInterface::EStatsValue::FAIL_TO_READ_ERROR)        << "\n";

    std::cout << rcv_iface.getStatistics(kord::ReceiverInterface::EStatsValue::CMD_JITTER_MAX_LOCAL)/1000. << " ";
    std::cout << rcv_iface.getStatistics(kord::ReceiverInterface::EStatsValue::CMD_JITTER_AVG_LOCAL)/1000. << " ";
    std::cout << rcv_iface.getStatistics(kord::ReceiverInterface::EStatsValue::ROUND_TRIP_TIME_AVG_LOCAL)/1000. << " ";
    std::cout << std::endl;

    
    
    return 0;
}
