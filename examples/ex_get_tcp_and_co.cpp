//
//  main.cpp
//  kord_prototype
//
//  Created by Martin Straka on 26.01.2022.
//

#include <iostream>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;

volatile bool stop = false;

void signal_handler( sig_atomic_t a_signum ) {
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

int main(int argc, char * argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        lp.printUsage(false);
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));
    
    // insert code here...

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);


    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    // TODO: prepare initial q
    std::vector<double> q;
    q.resize(7);
    
    // Obtain initial q values
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 6UL> fetched_TCP = rcv_iface.getTCP();

        if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    fetched_TCP = rcv_iface.getTCP();
    
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    fetched_TCP = rcv_iface.getTCP();

    std::cout << "TCP: [ ";
    for( double p: fetched_TCP )
        std::cout << p << " ";
    std::cout << "]" << std::endl;


    int counter = 0;

    // refresh frequency
    
    while(!stop){
        std::cout << "Update " << counter++ << "\n";
        
        if (!kord->waitSync(std::chrono::milliseconds(10), kord::F_SYNC_FULL_ROTATION)){
            std::cout << "Sync RC failed.\n";
            //return EXIT_FAILURE;
        }
        
        rcv_iface.fetchData();
        std::vector<std::variant<double, int>> tfc = rcv_iface.getFrame(kord::EFrameID::TFC_FRAME, kord::EFrameValue::POSE_VAL_REF_WF);
        std::cout << "TFC Pose in WF: [ ";
        for( auto p: tfc )
            std::cout << std::get<double>(p) << " ";
        std::cout << "]" << std::endl;
    
        std::vector<std::variant<double, int>> tcp = rcv_iface.getFrame(kord::EFrameID::TCP_FRAME, kord::EFrameValue::POSE_VAL_REF_TFC);
        std::cout << "TCP Pose in TFC: [ ";
        for( auto p: tcp ) std::cout << std::get<double>(p) << " ";
        std::cout << "]" << std::endl;
        
        std::vector<std::variant<double, int>> load1_cog = rcv_iface.getLoad(kord::ELoadID::LOAD1, kord::ELoadValue::COG_VAL);
        std::cout << "Load1.cog = [ ";
        for( auto val: load1_cog ) { std::cout << std::get<double>(val) << " "; }
        std::cout << "]" << std::endl;
        std::vector<std::variant<double, int>> load1_mass = rcv_iface.getLoad(kord::ELoadID::LOAD1, kord::ELoadValue::MASS_VAL);
        std::cout << "Load1.mas = ";
        for( auto val: load1_mass ) { std::cout << std::get<double>(val) << " "; }
        std::cout << std::endl;
        std::vector<std::variant<double, int>> load1_inertia = rcv_iface.getLoad(kord::ELoadID::LOAD1, kord::ELoadValue::INERTIA_VAL);
        std::cout << "Load1.inertia = [ ";
        for( auto val: load1_inertia ) { std::cout << std::get<double>(val) << " "; } std::cout << " ]";
        std::cout << std::endl;
        
        
        std::vector<std::variant<double, int>> load2_cog = rcv_iface.getLoad(kord::ELoadID::LOAD2, kord::ELoadValue::COG_VAL);
        std::cout << "Load2.cog = [ ";
        for( auto val: load2_cog ) { std::cout << std::get<double>(val) << " "; }
        std::cout << "]" << std::endl;
        std::vector<std::variant<double, int>> load2_mass = rcv_iface.getLoad(kord::ELoadID::LOAD2, kord::ELoadValue::MASS_VAL);
        std::cout << "Load2.mass = ";
        for( auto val: load2_mass ) { std::cout << std::get<double>(val) << " "; }
        std::cout << std::endl;
        std::vector<std::variant<double, int>> load2_inertia = rcv_iface.getLoad(kord::ELoadID::LOAD2, kord::ELoadValue::INERTIA_VAL);
        std::cout << "Load2.inertia = [ ";
        for( auto val: load2_inertia ) { std::cout << std::get<double>(val) << " "; } std::cout << " ]";
        std::cout << " ] ";

        std::cout << "\n\n";
        
    }
    
    
    

    
    return 0;
}
