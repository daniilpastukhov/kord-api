#include <iostream>
#include <iomanip>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>
#include <kord/utils/utils.h>
#include <kord/api/kord_io_request.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;
volatile bool g_run = true;

//! Will request the KORD Cbun to initiate and transfer IO status to target
int64_t requestIOSet(kord::KordCore&, kord::ControlInterface&);

//! Will monitor the status of the request to IO status transfer until time out, failure, or success
bool monitorIOSet(kord::KordCore&, kord::ReceiverInterface&, utils::LaunchParameters&, int64_t);

void signal_handler( int a_signum ) {
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char * argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        //lp.printUsage(true);
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler); 

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(true);
            return EXIT_FAILURE;
        }
    }
    
    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));


    kord::ControlInterface  ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);


    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    g_run = true;
    
    
    // Obtain initial q values
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

    std::cout << "Read initial joint configuration:\n";
    for( double angl: start_q )
        std::cout << (angl/3.14)*180 << " ";

    std::cout << "\n";

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    //
    if (!kord->waitSync(std::chrono::milliseconds(10))){
        std::cout << "Sync wait timed out, exit \n";
        return EXIT_FAILURE;
    }
    
    // Create a request to the remote controller
    kr2::kord::RequestIO io_request;
    io_request.asSetIODigitalOut()
              .withEnabledPorts( // Enable Relay 1, Digital Output 8, Digital Output 6 and Tool Board 1 [24V]
                kr2::kord::RequestIO::DIGITAL_RELAYS::RELAY1|
                kr2::kord::RequestIO::DIGITAL_IOBOARD::DO8|
                kr2::kord::RequestIO::DIGITAL_IOBOARD::DO6|
                kr2::kord::RequestIO::DIGITAL_IOTOOLB::TB1);

            //   .withDisabledPorts( // Disable Relay 1 and Digital Output 6 and Tool Board 1 [24V]
            //     kr2::kord::RequestIO::DIGITAL_RELAYS::RELAY1|
            //     kr2::kord::RequestIO::DIGITAL_IOBOARD::DO6|
            //     kr2::kord::RequestIO::DIGITAL_IOTOOLB::TB1);

    kord->sendCommand(io_request);
    std::cout << "TX Request    RID: " << io_request.request_rid_ << "\n";
    //
    // Waiting for the execution result
    //
    kr2::kord::RequestIO response;
    while(true){
        kord->waitSync(std::chrono::milliseconds(10));
        rcv_iface.fetchData();

        response = rcv_iface.getLatestRequest();

        if (lp.runtimeElapsed()){
            std::cout << "TIMEOUT: Request with RID " << response.request_rid_ << "\n";
            return EXIT_FAILURE;
        }

        // Check if the correct request is being evaluated
        if (io_request.request_rid_ != response.request_rid_) continue;

        if (response.request_status_ == kr2::kord::protocol::EControlCommandStatus::eSuccess){
            std::cout << "SUCCESS: Request with RID " << response.request_rid_<< ", transfer finished.\n";
            break;
        }
        
        if (response.request_status_ == kr2::kord::protocol::EControlCommandStatus::eFailure){
            std::cout << "FAILURE: Request with RID: " << response.request_rid_ << "\n";
            return EXIT_FAILURE;
        }
    }

    std::cout << "Done\n";
    return EXIT_SUCCESS;
}
