//
//  main.cpp
//  kord_prototype
//
//  Created by Martin Straka on 26.01.2022.
//

#include <iostream>
#include <iomanip>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>
#include <kord/utils/utils.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;
static bool g_run = true;

void signal_handler( int a_signum ) {
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char * argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        //lp.printUsage(true);
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler); 

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(true);
            return EXIT_FAILURE;
        }
    }
    
    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));


    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);


    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    std::array<double, 7UL> q;
    std::array<double, 7UL> degs;
    g_run = true;
    
    // Obtain initial q values
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

    std::cout << "[Angle]Read initial joint configuration:\n";
    for( double angl: start_q )
        std::cout << (angl/3.14)*180 << " ";

    std::cout << "\n";

    double tt_value  = 5.0;
    double bt_value  = 3.0;
    double sync_time = 10.0;

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    switch (lp.rpose)
    {
    case 0:
        degs[0] = 0.0; degs[1] = -15.0; degs[2] = 0.0;  degs[3] = 90.0;  degs[4] = 0.0;  degs[5] = 90.0; degs[6] = 0.0;
        break;
    
    case 1:
        degs[0] = 0.0; degs[1] = 0.0;   degs[2] = 30.0; degs[3] = 120.0; degs[4] = 30.0; degs[5] = 80.0; degs[6] = 10.0;
        break;
    
    case 2:
        degs[0] = 0.0; degs[1] = 25.0;  degs[2] = 50.0; degs[3] = 80.0;  degs[4] = 60.0; degs[5] = 60.0; degs[6] = 30.0;
        break;
    
    default:
        std::cout << "Robot pose is not defined. Go to default." << std::endl;
        degs[0] = 0.0; degs[1] = -15.0; degs[2] = 0.0; degs[3] = 90.0; degs[4] = 0.0; degs[5] = 90.0; degs[6] = 0.0;
        break;
    }
    
    if (!kord->waitSync(std::chrono::milliseconds(10))){
        std::cout << "Sync wait timed out, exit \n";
        return 1;
    }

    for(size_t i = 0; i < 7; i++) q[i] = degs[i]*3.1415/180.0;

    ctl_iface.moveJ(q,
                    kr2::kord::TrackingType::TT_TIME,    tt_value,
                    kr2::kord::BlendType::BT_TIME,       bt_value,
                    kr2::kord::OverlayType::OT_VIAPOINT,
                    sync_time);

    rcv_iface.fetchData();
    if (rcv_iface.systemAlarmState() ||
        lp.runtimeElapsed())
    {
        return 1;
    }

    std::cout << "Robot stopped\n";
    std::cout << rcv_iface.getFormattedInputBits()  << std::endl;
    std::cout << rcv_iface.getFormattedOutputBits() << std::endl;
    std::cout << "SystemAlarmState:\n";
    std::cout << rcv_iface.systemAlarmState() << "\n";
        switch (rcv_iface.systemAlarmState() & 0b1111){
        case kr2::kord::protocol::EEventGroup::eUnknown:
            std::cout << "No alarms" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eSafetyEvent:
            std::cout << "Safety Event" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eSoftStopEvent:
            std::cout << "Soft Stop Event" << '\n';
            break;
        case kr2::kord::protocol::EEventGroup::eKordEvent:
            std::cout << "Kord Event" << '\n';
            break;
    }

    std::cout << "Safety flags: " << rcv_iface.getRobotSafetyFlags() << "\n";
    std::cout << "Runtime: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count()/1000.0 << " [s]\n";
    std::cout << "SafetyFlags: " << rcv_iface.getRobotSafetyFlags() << "\n";
    std::cout << "MotionFlags: " << rcv_iface.getMotionFlags() << "\n";
    std::cout << "RCState: " << rcv_iface.getRobotSafetyFlags() << "\n";
    
    kord->printStats(rcv_iface.getStatisticsStructure());
    return 0;
}
