//
//  main.cpp
//  kord_prototype
//
//  Created by Martin Straka on 26.01.2022.
//

#include <iostream>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>

#include <csignal>
#include <chrono>
#include <sstream>

using namespace kr2;

volatile bool stop = false;

void signal_handler( sig_atomic_t a_signum ) {
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

int main(int argc, char * argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        //lp.printUsage(true);
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(true);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));
    
    // insert code here...

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);


    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    // TODO: prepare initial q
    std::vector<double> q;
    q.resize(7);
    
    // Obtain initial q values
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 7UL> temp_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_TEMP_BOARD);

    std::cout << "Joints Board Temperatures:";
    for( double temp: temp_q )
        std::cout << temp << ";";
    std::cout << std::endl;

    temp_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_TEMP_JOINT_ENCODER);

    std::cout << "Joints Encoder Temperatures:";
    for( double temp: temp_q )
        std::cout << temp << ";";
    std::cout << std::endl;


    temp_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_TEMP_ROTOR_ENCODER);

    std::cout << "Rotor encoder Temperatures:";
    for( double temp: temp_q )
        std::cout << temp << ";";
    std::cout << std::endl;

    if (!kord->waitSync(std::chrono::milliseconds(20))){
            std::cout << "Sync wait timed out, exit \n";
        }

    rcv_iface.fetchData();

    if (!kord->waitSync(std::chrono::milliseconds(20))){
            std::cout << "Sync wait timed out, exit \n";
    }

    rcv_iface.fetchData();
    

    if (!kord->waitSync(std::chrono::milliseconds(20))){
            std::cout << "Sync wait timed out, exit \n";
    }

    rcv_iface.fetchData();
    

    if (!kord->waitSync(std::chrono::milliseconds(20))){
            std::cout << "Sync wait timed out, exit \n";
    }

    rcv_iface.fetchData();
    


    double tempIO_q = rcv_iface.getIOBoardTemperature();

    std::cout << "IOBoard Temperature: " << tempIO_q;
    std::cout << std::endl;

    std::cout << "CPU Temperatures" << '\n';    
    std::cout << "(Zeroes returned if **lm-sensors version 1:3.4.0-4** is not installed to the controller) \n";
    // check
    std::cout << "Pack 0: " << rcv_iface.getCPUState(kord::ReceiverInterface::ECPUStateValue::PACKAGE_ID0_TEMP) << std::endl;
    std::cout << "Core 0: " << rcv_iface.getCPUState(kord::ReceiverInterface::ECPUStateValue::CORE_0_TEMP) << std::endl;
    std::cout << "Core 1: " << rcv_iface.getCPUState(kord::ReceiverInterface::ECPUStateValue::CORE_1_TEMP) << std::endl;
    std::cout << "Core 2: " << rcv_iface.getCPUState(kord::ReceiverInterface::ECPUStateValue::CORE_2_TEMP) << std::endl;
    std::cout << "Pack 3: " << rcv_iface.getCPUState(kord::ReceiverInterface::ECPUStateValue::CORE_3_TEMP) << std::endl;

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    std::cout << "Runtime, SF, SSTOP; PSTOP, ESTOP\n";
    std::chrono::time_point<std::chrono::steady_clock, std::chrono::seconds> print_stamp = std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::steady_clock::now());
    
    // Infinite loop
    while(true) {
        //
        // calculation
        // update q
        //

        if (!kord->waitSync(std::chrono::milliseconds(10))){
            std::cout << "Sync wait timed out, exit \n";
            break;
        }

        rcv_iface.fetchData();
        auto now_ts = std::chrono::steady_clock::now();
        if ((print_stamp <  std::chrono::time_point_cast<std::chrono::seconds>(now_ts)) && (std::chrono::duration_cast<std::chrono::seconds>(now_ts - start).count() % 2))
        {
            print_stamp = std::chrono::time_point_cast<std::chrono::seconds>(now_ts);
            std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() << ", ";
            std::cout << rcv_iface.getRobotSafetyFlags() << ", ";
            std::cout << rcv_iface.systemAlarmState() << "\n";
        }

        if (lp.runtimeElapsed()){
            break;
        }
        if (stop) { break; }
    }

    kord->printStats(rcv_iface.getStatisticsStructure());
    
    return 0;
}
