//
//  main.cpp
//  - kord_prototype
//  - retrieve errors test
//  - this test print the most recent system errors
//
//

#include <iostream>
#include <iomanip>
#include <cmath>

#include <kord/api/kord_receive_interface.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord.h>
#include <kord/utils/utils.h>
#include <kord/api/api_request.h>

#include <csignal>
#include <chrono>
#include <sstream>
#include <thread>

using namespace kr2;
static bool g_run = true;

void signal_handler( int a_signum ) {
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char * argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        //lp.printUsage(false);
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler); 

    if (lp.useRealtime()){
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)){
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(false);
            return EXIT_FAILURE;
        }
    }
    
    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(new kord::KordCore(
        lp.remote_controller_,
        lp.port_,
        lp.session_id_,
        kord::UDP_CLIENT));


    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);


    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    g_run = true;
    
    // Obtain initial q values
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

    std::cout << "Heartbeat captured, read joint configuration:\n";
    for( double angl: start_q )
        std::cout << (angl/3.14)*180 << " ";
    std::cout << std::endl;

    while (g_run) 
    {
         if (!kord->waitSync(std::chrono::milliseconds(10))){
            std::cout << "Sync wait timed out, exit \n";
            break;
        }
        rcv_iface.fetchData();

        std::cout << "Read the last system event: " << std::endl;

        std::vector<kr2::kord::protocol::SystemEvent> events = rcv_iface.getSystemEvents();
        if (events.size() > 0)
        {
            for (std::vector<int>::size_type idx = 0; idx < events.size(); ++idx)
                if (idx < 1) std::cout << events[idx].toString() << std::endl;
                else         std::cout << events[idx].toStringWithRef(events[idx-1]) << std::endl;
        }
        // else std::cout << "No errors.";
        std::cout << std::endl;

        rcv_iface.clearSystemEventsBuffer();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    std::cout << "Done\n";

    return EXIT_SUCCESS;
}




